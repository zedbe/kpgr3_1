package modely;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import common.GridFactory;
import oglutils.*;
import transforms.*;

import java.awt.event.*;
import java.nio.ByteBuffer;

/**
 * GLSL sample:<br/>
 * Read and compile shader from files "/shader/glsl01/start.*" using ShaderUtils
 * class in oglutils package <br/>
 * Manage (create, bind, draw) vertex and index buffers using OGLBuffers class
 * in oglutils package<br/>
 * Requires JOGL 2.3.0 or newer
 * 
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2015-09-05
 */
public class Renderer implements GLEventListener, MouseListener,
		MouseMotionListener, KeyListener {

	private OGLBuffers buffers;
	private OGLTextRenderer textRenderer;

	private int shaderProgram, locTime, locView, locProj, locShapeSet, locRotation, locX, locY;

	private float time = 0;
	private Mat4 proj;
	private Camera camera;
	private int mx, my, width, height, cooX, cooY;
	private int shapeSet = 0, rotate = 0;
	private boolean polygons = true, coordCapture = false, isPerspective = true;


	@Override
	public void init(GLAutoDrawable glDrawable) {
		// check whether shaders are supported
		GL2GL3 gl = glDrawable.getGL().getGL2GL3();
		OGLUtils.shaderCheck(gl);
		
		OGLUtils.printOGLparameters(gl);
		
		textRenderer = new OGLTextRenderer(gl, glDrawable.getSurfaceWidth(), glDrawable.getSurfaceHeight());

		// shader files are in /shaders/ directory
		// shaders directory must be set as a source directory of the project
		// e.g. in Eclipse via main menu Project/Properties/Java Build Path/Source

		shaderProgram = ShaderUtils.loadProgram(gl, "/start");

		buffers = GridFactory.generateGrid(gl, 50, 50);

		locProj = gl.glGetUniformLocation(shaderProgram, "proj");
		locView = gl.glGetUniformLocation(shaderProgram, "view");
		locShapeSet = gl.glGetUniformLocation(shaderProgram, "shapeSet");
		locRotation = gl.glGetUniformLocation(shaderProgram, "rotate");
		locTime = gl.glGetUniformLocation(shaderProgram, "time");
		locX = gl.glGetUniformLocation(shaderProgram, "cooX");
		locY = gl.glGetUniformLocation(shaderProgram, "cooY");

		camera = new Camera()
				.withPosition(new Vec3D(5, 5, 2.5))
				.withAzimuth(Math.PI * 1.25)
				.withZenith(Math.PI * -0.125);


		gl.glEnable(GL2GL3.GL_DEPTH_TEST);

	}

	
	@Override
	public void display(GLAutoDrawable glDrawable) {
		GL2GL3 gl = glDrawable.getGL().getGL2GL3();
		
		gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);
		
		// set the current shader to be used, could have been done only once
		gl.glUseProgram(shaderProgram);

		time += 0.025;

		gl.glUniform1f(locTime, time); // correct shader must be set before this
		gl.glUniformMatrix4fv(locView, 1,false, camera.getViewMatrix().floatArray(),0);
		gl.glUniformMatrix4fv(locProj,1,false, proj.floatArray(),0);
        gl.glUniform1i(locShapeSet, shapeSet);
		gl.glUniform1i(locRotation, rotate);
		gl.glUniform1i(locX, cooX);
		gl.glUniform1i(locY, cooY);


		// LEGENDA
		String text = "[WSAD,LMB,ctrl,shift] camera ; [1] model switch";

		if (polygons){
			gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, GL2GL3.GL_FILL);
			text +="; [m]ode: fill";
		} else{
			gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, GL2GL3.GL_LINE);
			text +="; [m]ode: edges";
		}


		buffers.draw(GL2GL3.GL_TRIANGLE_STRIP, shaderProgram);	// bind and draw
		textRenderer.drawStr2D(3, height - 20, text);

		if (rotate == 0) textRenderer.drawStr2D(3, height - 35, "[r]otate: off");
		else textRenderer.drawStr2D(3, height - 35, "[r]otate: on");

		if (isPerspective) textRenderer.drawStr2D(3, height - 50, "[p]rojection: perspective");
		else  textRenderer.drawStr2D(3, height - 50, "[p]rojection: orthogonal");

		// DEBUG
		gl.glFlush();

		ByteBuffer colorBuffer = ByteBuffer.allocate(3);
		gl.glReadPixels(cooX, cooY, 1, 1, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, colorBuffer);
		byte[] signedRGB = colorBuffer.array();			// signed bytes
		int[] unsignedRGB = new int[signedRGB.length];	// to ints
		for (int i = 0; i < signedRGB.length; i++) {
			unsignedRGB[i] = signedRGB[i] & 0xFF;		// convert into unsigned
		}
		if (coordCapture) {
			textRenderer.drawStr2D(3, 27, "X: " + cooX);
			textRenderer.drawStr2D(3, 15, "Y: " + cooY);
			textRenderer.drawStr2D(3, 3, "RGB: "
					+ unsignedRGB[0] + ", "
					+ unsignedRGB[1] + ", "
					+ unsignedRGB[2]);
		}

		// PODPIS
		textRenderer.drawStr2D(width - 110, 3, "2019 (c) UHK FIM");
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		this.width = width;
		this.height = height;

		projectionSwitcher();

		textRenderer.updateSize(width, height);
	}

	private void projectionSwitcher(){
		double ratio = height / (double) width;

		if (isPerspective) {
			proj = new Mat4PerspRH(Math.PI / 4, ratio, 0.1, 1000);

		}else {
			proj = new Mat4OrthoRH(Math.PI * 4, Math.PI * 4 * ratio, 0.1, 1000);
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		coordCapture = true;
		cooX = e.getX();
		cooY = e.getY();
		System.out.println("Coordinates [X: " + cooX + ", Y: " + cooY + "]");
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		mx = e.getX();
		my = e.getY();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		camera = camera.addAzimuth(Math.PI * (mx - e.getX()) / width)
				.addZenith(Math.PI * (e.getY() - my) / width);
		mx = e.getX();
		my = e.getY();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
			case KeyEvent.VK_W:
				camera = camera.forward(1);
				break;
			case KeyEvent.VK_D:
				camera = camera.right(1);
				break;
			case KeyEvent.VK_S:
				camera = camera.backward(1);
				break;
			case KeyEvent.VK_A:
				camera = camera.left(1);
				break;
			case KeyEvent.VK_CONTROL:
				camera = camera.down(1);
				break;
			case KeyEvent.VK_SHIFT:
				camera = camera.up(1);
				break;
			case KeyEvent.VK_1:
				if(shapeSet == 5) shapeSet = 0;
				else shapeSet++;
				break;
			case KeyEvent.VK_M:
				polygons = !polygons;
				break;
            case KeyEvent.VK_R:
                if(rotate == 1) rotate = 0; // on/off
                else rotate = 1;
                break;
			case KeyEvent.VK_P:
				isPerspective = !isPerspective;
				projectionSwitcher();
				break;
            case KeyEvent.VK_ESCAPE:
				System.exit(0);
				break;
        }
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void dispose(GLAutoDrawable glDrawable) {
		GL2GL3 gl = glDrawable.getGL().getGL2GL3();
		gl.glDeleteProgram(shaderProgram);
	}

}