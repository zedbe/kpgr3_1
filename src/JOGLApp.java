import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class JOGLApp {
	private static final int FPS = 60; // animator's target frames per second

	private GLCanvas canvas = null;

	private int demoId = 1;


	private static String[] names = { // edit and comment out the call getDemoNames in start to cherry-pick the samples to be listed
			"modely", "phong"
	};
	private static int[] countMenuItems = {15,20};
	private static String[] nameMenuItem = {"ukol"};
	private KeyAdapter keyAdapter;

	public void start() {
		try {
			Frame testFrame = new Frame("TestFrame");
			testFrame.setSize(512, 384);

			makeGUI(testFrame);

			setApp(testFrame, names[0]);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void makeGUI(Frame testFrame) {
		ActionListener actionListener = ae -> {
			demoId = Integer
					.valueOf(ae.getActionCommand().substring(0, ae.getActionCommand().lastIndexOf('-') - 1).trim());
			setApp(testFrame, names[demoId - 1]);
		};

		MenuBar menuBar = new MenuBar();
		int menuIndex = 0;
		for(int itemMenu = 0 ; itemMenu < nameMenuItem.length; itemMenu++){
			Menu menu1 = new Menu(nameMenuItem[itemMenu]);
			MenuItem m;
			for (int i = 0; i < names.length && i < countMenuItems[itemMenu]; i++) {
				m = new MenuItem((menuIndex + 1) + " - "
						+ names[menuIndex]);
				m.addActionListener(actionListener);
				menu1.add(m);
				menuIndex++;
			}
			menuBar.add(menu1);
		}

		keyAdapter = new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
					case KeyEvent.VK_HOME:
						demoId = 1;
						setApp(testFrame, names[demoId - 1]);

						break;
					case KeyEvent.VK_END:
						demoId = names.length;
						setApp(testFrame, names[demoId - 1]);
						break;
					case KeyEvent.VK_LEFT:
						if (demoId > 1)
							demoId--;
						setApp(testFrame, names[demoId - 1]);
						break;
					case KeyEvent.VK_RIGHT:
						if (demoId < names.length)
							demoId++;
						setApp(testFrame, names[demoId - 1]);
						break;
				}
			}

		};

		testFrame.setMenuBar(menuBar);
	}

	private void setApp(Frame testFrame, String name) {
		Dimension dim;
		if (canvas != null){
			testFrame.remove(canvas);
			dim = canvas.getSize();
		} else {
			dim = new Dimension(600, 400);
		}
			
		// setup OpenGL version
		GLProfile profile = GLProfile.getMaximum(true);
		GLCapabilities capabilities = new GLCapabilities(profile);
		capabilities.setRedBits(8);
		capabilities.setBlueBits(8);
		capabilities.setGreenBits(8);
		capabilities.setAlphaBits(8);
		capabilities.setDepthBits(24);

		canvas = new GLCanvas(capabilities);
		canvas.setSize(dim);
		testFrame.add(canvas);

		Object ren = null;
		Class<?> renClass;
		try {
			renClass = Class.forName(name + ".Renderer");
			ren = renClass.newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e1) {
			e1.printStackTrace();
		}

		canvas.addGLEventListener((GLEventListener) ren);
		canvas.addKeyListener((KeyListener) ren);
		canvas.addKeyListener(keyAdapter);
		canvas.addMouseListener((MouseListener) ren);
		canvas.addMouseMotionListener((MouseMotionListener) ren);
		canvas.requestFocus();

		final FPSAnimator animator = new FPSAnimator(canvas, FPS, true);
		testFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				new Thread(() -> {
					if (animator.isStarted())
						animator.stop();
					System.exit(0);
				}).start();
			}
		});
		
		testFrame.setTitle("1. ukol KPGRF3");

		testFrame.pack();
		testFrame.setVisible(true);
		animator.start(); // start the animation loop
}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new JOGLApp().start());
	}

}