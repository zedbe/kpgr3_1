#version 150
in vec2 inPosition; // input from the vertex buffer
//in vec3 inColor; // input from the vertex buffer
out vec3 vertColor; // output from this shader to the next pipeline stage
out vec3 vertNormal; // output from this shader to the next pipeline stage

uniform float time;     // variable constant for all vertices in a single draw
uniform mat4 proj;
uniform mat4 view;
uniform int shapeSet;   // model switcher
uniform int rotate;     // rotation indicator to use time
uniform int cooX;
uniform int cooY;

const float PI = 3.141592;

// grid ve tvaru zeme
vec3 getGround(vec2 xy)
{
    return vec3(xy, 0);
}

// grid vetvaru zdi
vec3 getWall(vec2 xy) {
    return vec3(xy.x , 0, xy.y);
}

// kartezske souradnice /////////////////////////////////////////
// kuzel
vec3 getCone(vec2 xy) {
    float s = 2 * PI * xy.x;
    float t = xy.y;

    if (rotate == 1) s += time;

    float x = t * cos(s);
    float y = t * sin(s);
    float z = t;

    return vec3(x, y, z);
}

// koule
vec3 getSphere(vec2 xy) {
    float s = 2 * PI * xy.x;
    float t = PI * xy.y;

    if (rotate == 1) s += time;

    float x = sin(t)*cos(s);
    float y = sin(t)*sin(s);
    float z = cos(t);

    return vec3(x, y, z);
}

////////////////////////////////////////////////////////////////
// sfericke souradnice /////////////////////////////////////////
// vetrak
vec3 getFan(vec2 xy) {
    float azimuth =  2 * PI * xy.x ;
    float zenith = 2 * PI * xy.y;
    float rho = sin(4 * azimuth);

    if (rotate == 1) azimuth += time;

    float x = rho * sin(zenith) * cos(azimuth);
    float y = rho * sin(zenith) * sin(azimuth);
    float z = rho * cos(zenith);

    return vec3(x, y, z);
}

// kaca
vec3 getSpinningTop(vec2 xy) {
    float azimuth = 2 * PI * xy.x ;
    float zenith = PI * xy.y;
    float rho = 1 + 1.5 * cos(4 * zenith);

    if (rotate == 1) azimuth += time;

    float x = rho * sin(zenith) * cos(azimuth);
    float y = rho * sin(zenith) * sin(azimuth);
    float z = rho * cos(zenith);

    return vec3(x, y, z);
}

// cylindricke souradnice /////////////////////////////////////////
// sombrero
vec3 getSombrero(vec2 paramPos) {
    float theta = 2 * PI * paramPos.x;
    float t = 2 * PI * paramPos.y;
    float r = t;

    if (rotate == 1) theta += time;

    float x = r * cos(theta) * 0.5;
    float y = r * sin(theta) * 0.5;
    float z = 2 * sin(t) * 0.5;

    return vec3(x, y, z);
}

//fontana
vec3 getFontain(vec2 paramPos) {
    float theta = 2 * PI * paramPos.x;
    float t = PI * paramPos.y ;
    float r = sin(cos(t) - 0.5) * 0.8;

    if (rotate == 1) theta += time;

    float x = r * cos(theta);
    float y = r * sin(theta);
    float z = sin(t);

    return vec3(x, y, z);
}

vec3 modelPicker(vec2 paramPos) {
    switch(shapeSet) {
        case 0:
        return getCone(paramPos);
        case 1:
        return getSphere(paramPos);
        case 2:
        return getFan(paramPos);
        case 3:
        return getSpinningTop(paramPos);
        case 4:
        return getSombrero(paramPos);
        case 5:
        return getFontain(paramPos);
    }
    return getCone(paramPos);
}

vec3 normal(vec2 paramPos) {
    float d = 1e-5;
    vec2 dx = vec2(d, 0);
    vec2 dy = vec2(0, d);
    vec3 tx = (modelPicker(paramPos + dx) - modelPicker(paramPos - dx)) / (2 * d);
    vec3 ty = (modelPicker(paramPos + dy) - modelPicker(paramPos - dy)) / (2 * d);
    return cross(ty, tx);
}

void main() {
	vec3 position = modelPicker(inPosition);


    vertNormal = normal(inPosition);

    vertColor = position;

    gl_Position = proj * view * vec4(position, 1.0);
}
