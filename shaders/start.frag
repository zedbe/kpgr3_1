#version 150
in vec3 vertColor; // input from the previous pipeline stage
in vec3 vertNormal;

out vec4 outColor; // output from the fragment shader

void main() {
//	outColor = vec4(normalize(vertNormal) * 0.5 + 0.5, 1.0);

	outColor = vec4(vertColor, 1.0);
} 
